<?php

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    /**
     * @inject
     * @var \DB\UserRepository
     */
    public $usersRepository;

    /**
     * @inject
     * @var \Utils\Email\Email
     */
    public $emailHelper;

    protected $date;
    protected $newRegistrationCount;

    // startup method
    protected function startup()
    {
        parent::startup();

        if (!($this->presenter->name == "User" && $this->view == "registration")
            && !($this->presenter->name == "Homepage")
            && !$this->getUser()->isLoggedIn()
        ) {
            $request = $this->storeRequest();
            $this->redirect('Sign:in', $request);
        } elseif (!($this->presenter->name == "User" && ($this->view == "registration") || ($this->view == "edit"))
            && !($this->presenter->name == "Homepage")
            && !($this->user->isInRole(UserRoles::ADMIN) || $this->user->isInRole(UserRoles::REGISTRATOR))
        ) {
            throw new \Nette\Application\ForbiddenRequestException();
        }
        $this->date = new \DateTime();
        if ($this->user->isLoggedIn()) {
            $this->newRegistrationCount = $this->usersRepository->getNewRegistrationsCount($this->user->id);
        } else {
            $this->newRegistrationCount = null;
        }
        $this->template->newRegistrationCount = $this->newRegistrationCount;
    }

    // method for singing out
    public function handleSignOut()
    {
        $this->getUser()->logout();
        $this->redirect('Homepage:default');
    }
}
