<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $this->table("user")
		    ->addColumn("username", "string", ["limit" => 255])->addIndex("username", ["unique" => true])
		    ->addColumn("name", "string", ["limit" => 255])
		    ->addColumn("surname", "string", ["limit" => 255])
		    ->addColumn("password", "string", ["limit" => 255])
		    ->addColumn("email", "string", ["limit" => 255])
		    ->addColumn("phone", "string", ["limit" => 25])
		    ->addColumn("birthday", "date", ["null" => true])
		    ->addColumn("birthplace", "string", ["limit" => 255])
		    ->addColumn("address", "string", ["limit" => 255])
		    ->addColumn("birth_number", "string", ["limit" => 25])
		    ->addColumn("role", "enum", ["values" => ["admin", "user"]])
		    ->addColumn("active", "boolean",  ['default' => true])
		    ->addColumn("signed", "boolean",  ['default' => false])
		    ->addColumn("pr", "boolean",  ['default' => true])
		    ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
		    ->addColumn('updated', 'timestamp', ['null' => true, 'update' => 'CURRENT_TIMESTAMP'])
		    ->addColumn('last_user_check', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
		    ->create();
    }
}
