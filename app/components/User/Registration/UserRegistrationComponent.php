<?php

namespace App\Components;


use Nette\Application\UI\Form;
use Vodacek\Forms\Controls\DateInput;

/**
 * Class UserRegistrationComponent
 * @package App\Components
 */
class UserRegistrationComponent extends BaseComponent
{
	/**
	 * @inject
	 * @var \DB\UserRepository
	 */
	public $userRepository;

    /**
     * @var \Utils\Email\Email
     * @inject
     */
    public $emailHelper;

    public function __construct()
    {
        parent::__construct();
    }

    protected function createComponentUserRegistration()
    {
        $form = new Form();
        $form->addText('username', 'Školní username')
            ->addRule(Form::FILLED, 'Zadejte školní username.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka přihlašovacího jména je %d znaků', 20);

        $form->addPassword('password', 'Heslo')
            ->addRule(Form::FILLED, 'Zadejte heslo.')
	        ->setRequired();

        $form->addPassword('passwordCheck', 'Ověření hesla')
            ->addRule(Form::FILLED, 'Zadejte heslo.')
            ->addRule(Form::EQUAL, 'Hesla musí být stejné', $form['password'])
	        ->setRequired();

        $form->addText('name', 'Jméno')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka jména je %d znaků', 20)
	        ->setRequired();

        $form->addText('surname', 'Příjmení')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka příjmení je %d znaků', 20)
	        ->setRequired();

        $form->addText('email', 'E-mail')
            ->setEmptyValue('@')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka emailu je %d znaků', 30)
            ->addCondition(Form::FILLED)
            ->addRule(Form::EMAIL, 'Zadán neplatný email.')
	        ->setRequired();

        $form->addText('phone', 'Telefon')
            ->setEmptyValue('+420')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka telefonu je %d znaků', 15)
	        ->setRequired();

	    $form->addDate('birthday', 'Datum narození', DateInput::TYPE_DATE)
		    ->setRequired("Vyplňte datum narození");

	    $form->addText('birthplace', 'Místo narození')
		    ->addRule(Form::MAX_LENGTH, 'Maximální délka místa narození je %d znaků', 250)
		    ->setRequired()
		    ->setAttribute("placeholder", "Město");;

	    $form->addText('address', 'Trvalé bydliště')
		    ->addRule(Form::MAX_LENGTH, 'Maximální délka trvalého bydliště je %d znaků', 250)
		    ->setRequired()
            ->setAttribute("placeholder", "Ulice č.p., Město, PSČ");

	    $form->addCheckbox("pr", "Chci pomáhat na PR akcích FIT ČVUT")
		    ->setDefaultValue(true);

        $form->addSubmit('send', 'Registrovat')
            ->setAttribute('class', 'pure-button pure-button-primary');

        $form->onValidate[] = [$this, "validateUsername"];
        $form->onSuccess[] = [$this, "userRegistrationSubmitted"];
	    $form->elementPrototype->addAttributes(['class' => 'pure-form']);
        return $form;
    }

	public function validateUsername(Form $form)
	{
		if((in_array($form->values->username, $this->userRepository->findAll()->fetchPairs('id', 'username')))) {
			$form->addError("Uživatel s tímto přihlašovacím jménem již existuje");
		}
	}

    public function userRegistrationSubmitted(Form $form)
    {
        $values = $form->getValues();

        $template = parent::createTemplate();
        $template->setFile(__MAIL_DIR__ . '/User/registration.latte');

        $this->userRepository->createUser($values);

	    $template = parent::createTemplate();
	    $template->setFile(__MAIL_DIR__ . '/User/registration.latte');
	    $template->username = $values->username;
	    $this->emailHelper->sendMailFromTemplate($template, "Registrace", [$values->email]);

	    $this->getPresenter()->flashMessage('Registrace proběhla úspěšně.', 'success');
	    $this->getPresenter()->redirect('Sign:in');
    }
}
