<?php

namespace App\Components;


use Nette\Application\UI\Form;
use Tracy\Debugger;
use Vodacek\Forms\Controls\DateInput;

/**
 * Class UserEditComponent
 * @package App\Components
 */
class UserEditSignedTimeComponent extends BaseComponent
{

	/**
	 * @inject
	 * @var \DB\UserRepository
	 */
	public $userRepository;

    private $userId;

    public function __construct($userId)
    {
        parent::__construct();

        $this->userId = $userId;
    }

    protected function createComponentUserEditSignedTime()
    {
        $editUser = $this->userRepository->findById($this->userId);

        $form = new Form();

	    $form->addDate('signed_time', 'Datum podpisu')
		    ->setRequired();

        $form->addHidden('id');

        $form->addSubmit('send', 'Editovat')
            ->setAttribute('class', 'pure-button pure-button-primary');

        $form->setDefaults($editUser);

        $form->onSuccess[] = [$this, "userSignedTimeEditSubmitted"];
	    $form->elementPrototype->addAttributes(['class' => 'pure-form']);
	    return $form;
    }

    public function userSignedTimeEditSubmitted(Form $form)
    {
        $values = $form->getValues();
        $this->userRepository->updateUser($values);
        $this->getPresenter()->flashMessage('Datum podpisu editováno.', 'success');
        $this->getPresenter()->redirect('default');
    }
}
