<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 17.7.15
 * Time: 9:59
 */

namespace App\Components;

/**
 * Interface IUserPasswordComponentFactory
 * @package App\Components
 */
interface IUserPasswordComponentFactory
{

    /** @return UserPasswordComponent */
    function create($userId);
}