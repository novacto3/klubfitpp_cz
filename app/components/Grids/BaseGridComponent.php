<?php


namespace App\Components;

use Grido\Components\Filters\Filter;
use Grido\Grid;

/**
 * Class BaseGridComponent
 * @package App\AdminModule\Components
 */
abstract class BaseGridComponent extends BaseComponent
{

	const ITEMS_PER_PAGE = 100;
	const NUMERIC_OPTIONS = ["0", ",", "&nbsp;"];
	const FILTER_NO_OPTION = ["" => ""];

	/**
	 * @var Grid
	 */
	protected $grid;

	/**
	 * @var string
	 */
	private $viewClass;


	/**
	 * BaseGridComponent constructor.
	 * @param string $viewClass
	 */
	public function __construct($viewClass = null)  // can be omitted to ensure a compatibility with old datagrids
	{
		parent::__construct();
		$this->viewClass = $viewClass;
	}


	/**
	 * @param string $name
	 * @param string $render
	 * @param int $itemsPerPage
	 */
	protected function initializeGrid($name, $render = Filter::RENDER_INNER, $itemsPerPage = self::ITEMS_PER_PAGE)
	{
		$this->grid = new Grid($this, $name);
		$this->grid->setFilterRenderType($render);
		$this->grid->setDefaultPerPage($itemsPerPage);
	}

	/**
	 * @param int $id
	 * @param string $column
	 * @param string $action
	 * @return string
	 */
	final protected function getDetailLink($id, $column = null, $action = "edit")
	{
		$link = $this->presenter->link($action, $id);
		if (empty($column)) {
			$column = "--nevyplněno--";
		}
		return "<a class=\"edit\" href=\" $link \"> $column </a>";
	}
}
