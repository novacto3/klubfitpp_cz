<?php

namespace App\Components;


use DB\UserRepository;
use Nette;
use Nette\Application\UI\Form;
use Utils\Email\Email;

/**
 * Class EmailEditableComponent
 * @package App\Components
 */
class EmailEditableComponent extends BaseComponent
{
	/**
	 * @inject
	 * @var \DB\UserRepository
	 */
	protected $userRepository;

	/**
	 * @inject
	 * @var \Utils\Email\Email
	 */
	private $emailHelper;

	private $mailCount = 20;

	public function __construct(UserRepository $userRepository, Email $emailHelper)
	{
		parent::__construct();

		$this->userRepository = $userRepository;
		$this->emailHelper = $emailHelper;
	}

	public function createComponentEmailEditable()
	{
		$form = new Form();
		$form->addText('subject', 'Předmět')
			->setRequired('Je nutné vyplnit předmět!');

		$form->addTextArea('content', 'Obsah emailu')
			->setRequired('Je nutné vyplnit obsah emailu!');

		$form->addSubmit('send', 'Odeslat')
			->setAttribute('class', 'btn');

		$form->onSuccess[] = [$this, "editableEmailSubmitted"];
		return $form;
	}

	public function editableEmailSubmitted(Form $form)
	{
		$values = $form->getValues();

		$session = $this->getPresenter()->getSession();
		$ids = $session->getSection("userIds")->ids;

		$users = $this->userRepository->findBy(['id' => $ids])->order('surname');
		/** @var Nette\Mail\Message $mail*/
		$mail = $this->prepareEmail($values);

		$counter = 0;
		foreach ($users as $user) {
			$mail->addBcc($user->email);
			$counter++;
			if ($counter % $this->mailCount == 0) {
				try {
					$this->emailHelper->smtpMailer->send($mail);
					$this->getPresenter()->flashMessage('Zpráva č. ' . ceil($counter / $this->mailCount) . ' z ' . ceil(count($users) / $this->mailCount) . ' úspěšně odeslána!', 'success');
				} catch (Nette\InvalidStateException $e) {
					$form->addError('Nepodařilo se odeslat e-mail.');
					$this->getPresenter()->flashMessage('Jednu ze zpráv se nepodařilo odeslat', 'error');
					\Tracy\Debugger::log($e, \Tracy\Debugger::ERROR);
				}
				/** @var Nette\Mail\Message $mail*/
				$mail = $this->prepareEmail($values);
			}
		}
		if ($counter % $this->mailCount) {
			try {
				$this->emailHelper->smtpMailer->send($mail);
				$this->getPresenter()->flashMessage('Zpráva č. ' . ceil($counter / $this->mailCount) . ' z ' . ceil(count($users) / $this->mailCount) . ' úspěšně odeslána!', 'success');
			} catch (Nette\InvalidStateException $e) {
				$form->addError('Nepodařilo se odeslat e-mail.');
				$this->getPresenter()->flashMessage('Jednu ze zpráv se nepodařilo odeslat', 'error');
				\Tracy\Debugger::log($e, \Tracy\Debugger::ERROR);
			}
		}

		$this->getPresenter()->redirect("User:default");
	}

	private function prepareEmail($values)
	{
		$mail = new Nette\Mail\Message();
		$mail->setSubject($values['subject']);
		$mail->setFrom('predstavenstvo@klubfitpp.cz', 'FIT++');
		$mail->setHtmlBody($values['content']);
		return $mail;
	}


	public function render()
	{
		$this->template->setFile(__DIR__ . '/EmailEditableComponent.latte');

		$session = $this->getPresenter()->getSession();
		$ids = $session->getSection("userIds")->ids;

		$users = $this->userRepository->findBy(['id' => $ids])->order('surname');

		$this->template->users = $users;
		$this->template->chars = [".", "-", " ", "="];
		$this->template->render();

	}
}
