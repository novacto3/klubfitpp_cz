<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 17.7.15
 * Time: 9:59
 */

namespace App\Components;

/**
 * Interface IUserRegistrationComponentFactory
 * @package App\Components
 */
interface IUserRegistrationComponentFactory
{

    /** @return UserRegistrationComponent */
    function create();
}