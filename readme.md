Using Docker-compose for development.

## Requirements:
- [direnv](https://direnv.net)
- Docker
- Docker-compose
- Nothing else, you really do not need local php
    
## To start:
- `cp .envrc.dist .envrc`, and after verifying content run `direnv allow`. This will add proper php to your path.
- `make start`
    - It will copy `docker-compose.override.dist.yaml` to `docker-compose.override.yaml`, in this file you can modify on which localhost ports services runnig. The default is:
        - [:8000 - web](http://localhost:8000)
        - [:8001 - adminer](http://localhost:8001) - DB editor
        - [:8002 - mailgon](http://localhost:8002) - Local mailtrap web gui
    - It will start up containers
    
- `make stop` to stop containers
- `make migrate` to run DB migrations
- `make help` for others
