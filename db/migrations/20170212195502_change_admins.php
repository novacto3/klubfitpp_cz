<?php

use Phinx\Migration\AbstractMigration;

class ChangeAdmins extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

	    $this->table("user")
		    ->changeColumn("role", "enum", ["values" => ["admin", "user", "registrator"]])
		    ->update();

    	$this->execute("UPDATE `user` SET `role` = 'user', `signed_time` = NULL WHERE `user`.`id` = 2");
    	$this->execute("UPDATE `user` SET `role` = 'admin', `signed_time` = NULL WHERE `user`.`id` = 6");
    	$this->execute("UPDATE `user` SET `role` = 'registrator', `signed_time` = NULL WHERE `user`.`id` = 12");

    }
}
