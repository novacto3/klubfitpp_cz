<?php

namespace App\Components;


use DB\UserRepository;
use Nette\Application\UI\Form;

/**
 * Class UserPasswordComponent
 * @package App\Components
 */
class UserPasswordComponent extends BaseComponent
{
	/**
	 * @inject
	 * @var \DB\UserRepository
	 */
	public $userRepository;

    private $userId;

    public function __construct($userId)
    {
        parent::__construct();

        $this->userId = $userId;
    }

    public function createComponentUserPassword()
    {
        $form = new Form();
        $form->addText('old', 'Staré heslo')
            ->setRequired('Zadejte své staré heslo')
            ->setAttribute('type', 'password');

        $form->addText('new', 'Nové heslo')
            ->setAttribute('type', 'password')
            ->setRequired('Zadejte nové heslo');

        $form->addText('newagain', 'Nové heslo znovu')
            ->setAttribute('type', 'password')
            ->addRule(Form::EQUAL, 'Zadané hesla se neshodují', $form['new'])
            ->setRequired('Zadejte nové heslo znovu');

        $form->addSubmit('submit', 'Editovat heslo')
            ->setAttribute('class', 'pure-button pure-button-primary');

        $form->onSuccess[] = [$this, "userPasswordSucceeded"];
	    $form->elementPrototype->addAttributes(['class' => 'pure-form']);
	    return $form;
    }

    public function userPasswordSucceeded(Form $form)
    {
        $values = $form->getValues();
        if ($values['new'] != $values['newagain']) {
            $this->getPresenter()->flashMessage('Nové heslo se neshoduje s kontrolním.', 'error');
            $this->getPresenter()->redirect('this');
        }
        if (!$this->userRepository->checkPasswordById($this->userId, $values['old'])) {
            $this->getPresenter()->flashMessage('Zadáno špatné staré heslo.', 'error');
            $this->getPresenter()->redirect('this');
        }
        $this->userRepository->changePasswordById($this->userId, $values['new']);
        $this->getPresenter()->flashMessage('Heslo úspěšně změněno.', 'success');
        $this->getPresenter()->redirect('this');
    }
}
