<?php

namespace App\Components;

/**
 * Interface IUserGridComponentFactory
 * @package App\Components
 */
interface IUserGridComponentFactory
{
	/**
	 * @return UserGridComponent
	 */
	public function create();
}
