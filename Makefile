help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-13s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

start: docker-compose.yaml docker-compose.override.yaml ## Starts whole projects
	docker-compose up -d --pull --remove-orphans

stop: ## Stops whole projects
	docker-compose stop

clean: ## Delete all containers and volumes. IT WILL DROP ALL DB DATA
	docker-compose down --remove-orphans --volumes

.envrc:
	cp .envrc.dist .envrc

docker-compose.override.yaml:
	cp docker-compose.override.dist.yaml docker-compose.override.yaml

migrate: start ## Run migrations
	vendor/bin/phinx migrate
