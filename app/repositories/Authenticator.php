<?php

use Nette\Security;
use Nette\Utils\Random;
use Nette\Utils\Strings;

/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Security\IAuthenticator
{
    /** @var Nette\Database\Context */
    protected $database;

    /**
     * @param \Nette\Database\Context $database
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $row = $this->database->table('user')->where('username', $username)->fetch();

        if (!$row) {
            throw new Security\AuthenticationException('Zadáno špatné uživatelské jméno nebo heslo.', self::IDENTITY_NOT_FOUND);
        }

        if ($row->password !== $this->calculateHash($password, $row->password)) {
            throw new Security\AuthenticationException('Zadáno špatné uživatelské jméno nebo heslo.', self::INVALID_CREDENTIAL);
        }

        if (!$row->active) {
            throw new Security\AuthenticationException('Uživatel není povolen.', self::INVALID_CREDENTIAL);
        }
        return new Security\Identity($row->id, $row->role, $row->toArray());
    }

    /**
     * Computes salted password hash.
     * @param  string
     * @return string
     */
    public static function calculateHash($password, $salt = null)
    {
        if ($password === Strings::upper($password)) {
            $password = Strings::lower($password);
        }
        return crypt($password, $salt ?: '$2a$07$' . Random::generate(22));
    }

}
