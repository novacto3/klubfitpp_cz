<?php

namespace App\Presenters;

use App\Components\UserGridComponent;
use App\Components\IUserGridComponentFactory;
use entities\User;
use Nette\Application\Responses\FileResponse;


class UserPresenter extends \BasePresenter
{

	/**
	 * @inject
	 * @var IUserGridComponentFactory
	 */
	public $userGridComponentFactory;

	/**
	 * @var \App\Components\IUserRegistrationComponentFactory
	 * @inject
	 */
	public $userRegistrationComponentFactory;

	/** @var \App\Components\IUserEditComponentFactory
	 * @inject
	 */
	public $userEditComponentFactory;

	/**
	 * @var \App\Components\IUserPasswordComponentFactory
	 * @inject
	 */
	public $userPasswordComponentFactory;

	/** @var \App\Components\IEmailEditableComponentFactory
	 * @inject */
	public $emailEditableComponentFactory;

	/** @var \App\Components\IUserEditSignedTimeComponentFactory
	 * @inject */
	public $userEditSignedTimeComponentFactory;

	private $id;

	public function actionDefault() {
		$show = true;
		if (!empty($this->getSignal())) {
			foreach ($this->getSignal() as $signal) {
				if ($signal === "signOut") {
					$show = false;
				}
			}
		}
		if ($this->newRegistrationCount !==0 && $show) {
			$this->flashMessage("Od tvé poslední návštěvy se nově registrovalo $this->newRegistrationCount nových lidí. <a href=\"" . $this->link("registrationsChecked") ."\"> Rozumím.</a>");
		}
	}

	public function actionRegistrationsChecked() {
		$this->usersRepository->setLastUserCheck($this->user->id);
		$this->redirect("default");
	}

	public function actionEditSignedTime($id) {
		/* @var User $user*/
		$user = $this->usersRepository->findById($id);
		$this->id = $id;
		$this->template->name = $user->name . " " . $user->surname;
	}

	public function actionDeactivate($id)
	{
		$this->usersRepository->deactivateUser($id);
		$this->redirect("default");
	}

	public function actionReactivate($id)
	{
		$this->usersRepository->activateUser($id);
		$this->redirect("default");
	}

	public function actionSign($id)
	{
		$this->usersRepository->setUsersSigned([$id]);
		$this->flashMessage("Uživatel úspěšně zapsán", "success");
		$this->redirect("default");
	}

	public function actionPrMailList()
	{
		$this->template->prUsers = $this->usersRepository->getPrMails();
	}

	public function actionSigned()
	{
		$session = $this->getSession();
		$ids = $session->getSection("userIds")->ids;
		$this->usersRepository->setUsersSigned($ids);
		$this->flashMessage("Uživatelé úspěšně zapsání", "success");
		$this->redirect("default");
	}

	public function actionSignForm()
	{
		$session = $this->getSession();
		$ids = $session->getSection("userIds")->ids;
		$users = $this->usersRepository->getUsers($ids);
		$parameters = [
			"users" => $users,
		];
		$html = (new \Latte\Engine())->renderToString(__PDF_DIR__.'/sign.latte', $parameters);
		$pdf = new \mPDF("", "A4-L");
		$pdf->WriteHTML($html);
		$date = date("j-n-Y");
		$filePath = __SIGN__. '/podpisy-FITpp-'.$date.'.pdf';
		$pdf->Output($filePath, 'F');
		$fileResponse = new FileResponse($filePath, basename($filePath), "application/pdf");
		$this->sendResponse($fileResponse);
	}


	/**
	 * @return UserGridComponent
	 */
	public function createComponentUserGrid()
	{
		return $this->userGridComponentFactory->create();
	}

	/**
	 * @return \App\Components\UserRegistrationComponent
	 */
	public function createComponentUserRegistration()
	{
		return $this->userRegistrationComponentFactory->create();
	}

	/**
	 * @return \App\Components\UserEditSignedTimeComponent
	 */
	public function createComponentUserEdit()
	{
		return $this->userEditComponentFactory->create($this->user->id);
	}

	/**
	 * @return \App\Components\UserPasswordComponent
	 */
	public function createComponentUserPassword()
	{
		return $this->userPasswordComponentFactory->create($this->user->id);
	}

	/**
	 * @return \App\Components\UserEditSignedTimeComponent
	 */
	public function createComponentUserEditSignedTime()
	{
		return $this->userEditSignedTimeComponentFactory->create($this->id);
	}

	/**
	 * @return \App\Components\EmailEditableComponent
	 */
	public function createComponentEmailEditable()
	{
		return $this->emailEditableComponentFactory->create();
	}

}
