<?php

namespace App\Components;


use Grido\Components\Filters\Filter;
use Grido\DataSources;
use Grido\Grid;
use Grido\Translations\FileTranslator;
use Tracy\Debugger;


/**
 * Class UserGridComponent
 * @package App\Components
 */
class UserGridComponent extends BaseGridComponent
{

	/**
	 * @var string
	 */
	const EXPORT_FILENAME = 'users';

	/**
	 * @inject
	 * @var \DB\UserRepository
	 */
	public $userRepository;

	/**
	 * @param string $name
	 * @return Grid
	 */
	protected function createComponentUserGrid($name)
	{

		$this->grid = new Grid($this, $name);
		$this->grid->setTranslator(new FileTranslator("cs"));

		// data source
		$model = new DataSources\NetteDatabase(
			$this->userRepository->findAll()
		);
		$this->grid->setModel($model);
		$this->grid->setDefaultPerPage(50);

		$this->grid->addColumnText("id", "ID")
			->setSortable()
			->setFilterNumber();

		$this->grid->getColumn('id')->cellPrototype->style['width'] = '5%';

		$this->grid->addColumnText("surname", "Příjmení")
			->setSortable()
			->setFilterText()
			->setSuggestion();

		$this->grid->addColumnText("name", "Jméno")
			->setSortable()
			->setFilterText();

		$this->grid->addColumnText("email", "E-mail")
			->setSortable()
			->setFilterText();

		$this->grid->addColumnText("username", "Login")
			->setSortable()
			->setFilterText();

		$this->grid->addColumnText("phone", "Telefon")
			->setSortable()
			->setFilterText();

		$this->grid->addColumnDate("birthday", "Datum narození")
			->setSortable()
			->setFilterDate();

		$this->grid->getColumn('birthday')->cellPrototype->style['width'] = '7%';

		$this->grid->addColumnText("pr", "Pomoc PR")
			->setSortable()
			->setCustomRender(function ($user) {
				return $user["pr"] == true ? "ANO" : "NE";
			})
			->setCustomRenderExport(function ($user) {
				return $user["pr"] == true ? "ANO" : "NE";
			})
			->setFilterSelect(["" => "", 1 => "ANO", 0 => "NE"])
			->setColumn("user.pr");

		/*$this->grid->addColumnText("role", "Role")
			->setSortable()
			->setFilterSelect($this->getRolesToFilterSelect());*/

		$this->grid->addColumnText("active", "Aktivní")
			->setSortable()
			->setCustomRender(function ($user) {
				return $user["active"] == true ? "ANO" : "NE";
			})
			->setCustomRenderExport(function ($user) {
				return $user["active"] == true ? "ANO" : "NE";
			})
			->setFilterSelect(["" => "", 1 => "ANO", 0 => "NE"])
			->setDefaultValue(1)
			->setColumn("user.active");

		$this->grid->addColumnText("signed", "Podpis")
			->setSortable()
			->setCustomRender(function ($user) {
				return $user["signed"] == true ? "ANO" : "NE";
			})
			->setCustomRenderExport(function ($user) {
				return $user["signed"] == true ? "ANO" : "NE";
			})
			->setFilterSelect(["" => "", 1 => "ANO", 0 => "NE"])
			->setColumn("user.signed");


		if ($this->presenter->user->isInRole(\UserRoles::ADMIN)) {

			// - deactivate button
			$this->grid->addActionHref("deactivate", "Deaktivovat")
				->setIcon("fa fa-remove")
				->setConfirm(function ($user) {
					return "Opravdu chcete deaktivovat uživatele \"" . $user["name"] . " " . $user["surname"] . "\"?";
				})
				->setDisable(function ($user) {
					if ($this->presenter->user->id === $user["id"]) {
						return true;
					}

					return !$user["active"];
				});

			// - activate button
			$this->grid->addActionHref("reactivate", "Aktivovat")
				->setIcon("check")
				->setConfirm(function ($user) {
					return "Opravdu chcete aktivovat uživatele \"" . $user["name"] . " " . $user["surname"] . "\" ? ";
				})
				->setDisable(function ($user) {
					return $user["active"];
				});
		}

		$this->grid->addActionHref("sign", "Podepsal")
			->setDisable(function ($user) {
				return $user["signed"];
			})
			->setConfirm(function ($user) {
				return "Opravdu uživatel \"" . $user["name"] . " " . $user["surname"] . "\" podepsal přihlášku? ";
			});

		$this->grid->addActionHref("editSignedTime", "UDP")
			->setDisable(function ($user) {
				return !$user["signed"];
			});

		$this->grid->filterRenderType = Filter::RENDER_INNER;
		$this->grid->setExport(self::EXPORT_FILENAME);

		$this->grid->setOperation([
			'signForm' => 'Podpisový formulář',
			'signed' => 'Podepsal',
			'email' => 'Hromadný email'
		], function($operation, $ids) {
			$session = $this->getPresenter()->getSession(); // returns the whole Session
			$section = $session->getSection('userIds');
			$section->ids = $ids;

			$this->getPresenter()->redirect("User:$operation");
		})->setConfirm("signed", "Opravdu chcete odškrnout uživatelům podepsání?");

		return $this->grid;
	}

	/**
	 * @return array
	 */
	private function getRolesToFilterSelect()
	{
		return [
			"" => "",
			\UserRoles::USER => "Uživatel",
			\UserRoles::ADMIN => "Admin",
			\UserRoles::REGISTRATOR => "Registrátor",
		];
	}
}
