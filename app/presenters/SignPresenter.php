<?php

namespace App\Presenters;

use Nette\Application\UI;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Utils\Random;
use Tracy\Debugger;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends UI\Presenter
{
    /**
     * @inject
     * @var \DB\UserRepository
     */
    public $userRepository;

    /**
     * @inject
     * @var \Utils\Email\Email
     */
    public $emailHelper;

    /**
     * @var String key that should be restored
     */
    private $key;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Sign-in form factory.
     */
    protected function startup()
    {
        parent::startup();
    }

    /**
     * logs in user
     * @param string $key string key of request that should be restored
     */
    public function actionIn($key = null)
    {
        $user = $this->getUser();
        $this->key = $key;

        if ($user->isLoggedIn()) {
            if ($key !== null) {
                if (!$this->restoreRequest($key)) {
                	$this->redirect("Homepage:default");
                };
            } else {
                $this->redirect('Homepage:default');
            }
        }
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->redirect('in');
    }

    public function registration()
    {
        $this->redirect('Users:registration');
    }

    public function forgot()
    {
        $this->redirect('forgot');
    }

    protected function createComponentSignInForm()
    {
        $form = new UI\Form;
        $form->addText('username', 'Login:')
            ->setRequired('Zadejte přihlašovací jméno.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Zadejte heslo.');

        $form->addCheckbox('remember', 'Zapamatovat');

        $form->addSubmit('send', 'Přihlásit')
            ->setAttribute('class', 'pure-button pure-button-primary');

        $form->addSubmit('forgot', 'Zapomněl(a) jsem heslo')
	        ->setValidationScope(false)
            ->setAttribute('class', 'btn')
            ->onClick[] = [$this, 'forgot'];

        $form->addSubmit('registration', 'Registrovat se')
	        ->setValidationScope(false)
	        ->setAttribute('class', 'pure-button btn-right')
            ->onClick[] = [$this, 'registration'];

        $form->onSuccess[] = [$this, "signInFormSucceeded"];
	    $form->elementPrototype->addAttributes(['class' => 'pure-form']);
	    return $form;
    }

    public function signInFormSucceeded(Form $form)
    {
        if ($form->isSubmitted() == $form['forgot']) {
        	$this->redirect('forgot');
        }
        $values = $form->getValues();

        if ($values->remember) {
            $this->getUser()->setExpiration('+ 10080 minutes', false);
        } else {
            $this->getUser()->setExpiration('+ 20 minutes', true);
        }

        try {
            $this->getUser()->login($values->username, $values->password);
            Debugger::log('Prihlaseni uzivatele ' . $values->username, "USERACCESS");
	        if ($this->user->isInRole("admin")) {
		        $this->getPresenter()->redirect('User:default');
	        } else {
		        $this->getPresenter()->redirect('Homepage:default');
	        }
        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
            return;
        }
    }

    protected function createComponentPasswordReset()
    {
        $form = new UI\Form;

        $form->addText('username', 'Přihlašovací jméno')
            ->setRequired('Zadejte svoje přihlašovací jméno.');

        $form->addSubmit('submit', 'Požádat o nové heslo')
            ->setAttribute('class', 'pure-button pure-button-primary');

        $form->onSuccess[] = [$this, "passwordResetFormSucceeded"];
	    $form->elementPrototype->addAttributes(['class' => 'pure-form']);
	    return $form;
    }

    public function passwordResetFormSucceeded(Form $form)
    {
        $values = $form->getValues();
        if (!$this->userRepository->getIDByUsername($values->username)) {
            $this->flashMessage('Takové uživatelské jméno neexistuje!', 'error');
            $this->redirect('forgot');
        }
        $password = Random::generate(22);
        $this->userRepository->changePassword($values->username, $password);
	    $template = $this->createTemplate();
	    $template->setFile(__MAIL_DIR__."/User/password.latte");
	    $template->password = $password;
        $email = $this->userRepository->getEmailByUsername($values->username);
        $this->emailHelper->sendMailFromTemplate($template, 'Reset hesla', [$email]);

        $this->flashMessage('Na tvůj mail ti bylo odesláno nové heslo.', 'success');
        $this->getPresenter()->redirect('Sign:in');
    }
}
