<?php
namespace DB;

use Nette\Utils\DateTime;
use Nette\Utils\Random;
use Nette\Utils\Strings;


/**
 * User Repository
 */
class UserRepository extends Repository
{
    // add user to database
    public function createUser($values)
    {
        $values->password = $this->calculateHash($values->password, null);
	    $values->role = "user";
	    $values->active = true;
        unset($values->passwordCheck);
        return $this->getTable()->insert($values);
    }

    // update user in database    
    public function updateUser($values)
    {
        $this->findById($values["id"])->update($values);
    }

	public function getUsers($ids)
	{
		return $this->findAll()->where("id", $ids);
	}

    // activate user
    public function activateUser($ids)
    {
        $this->findAll()->where("id", $ids)->update(['active' => 1]);
    }

    // deactivate user
    public function deactivateUser($ids)
    {
        $this->findAll()->where("id", $ids)->update(['active' => 0]);
    }

	// users signed form
	public function setUsersSigned($ids)
	{
		$this->findAll()->where("id", $ids)->update(['signed' => 1, 'signed_time' => new DateTime()]);
	}

	public function getPrMails() {
    	return $this->findAll()->where("pr", true);
	}


    // calculate hash of password
    public static function calculateHash($password, $salt = null)
    {
        if ($password === Strings::upper($password)) {
            $password = Strings::lower($password);
        }
        return crypt($password, $salt ?: '$2a$07$' . Random::generate(22));
    }

    public function changePassword($username, $password)
    {
        $this->findBy(['username' => $username])->update(['password' => $this->calculateHash($password)]);
    }

    public function changePasswordById($id, $password)
    {
        $this->findById($id)->update(['password' => $this->calculateHash($password)]);
    }

    public function checkPassword($username, $password)
    {
        $pass = $this->findOneBy(['username' => $username])['password'];
        if ($pass !== $this->calculateHash($password, $pass)) {
        	return false;
        } else {
        	return true;
        }
    }

    public function checkPasswordById($id, $password)
    {
        $pass = $this->findById($id)['password'];
        if ($pass !== $this->calculateHash($password, $pass)) {
        	return false;
        } else {
        	return true;
        }
    }

    public function getEmailByID($id)
    {
        return $this->findById($id)['email'];
    }

    public function getEmailByUsername($username)
    {
        return $this->findOneBy(['username' => $username])['email'];
    }

    public function getIDByUsername($username)
    {
        return $this->findOneBy(['username' => $username])['id'];
    }

    public function getUnsignedUsers()
    {
        return $this->findBy(['signed' => 0]);
    }

	public function setLastUserCheck($id)
	{
		return $this->findById($id)->update(["last_user_check" => new DateTime()]);
	}

	public function getNewRegistrationsCount($id)
	{
		$lastUserCheck = $this->findById($id)->offsetGet("last_user_check");

		return $this->findAll()->where("created >= ?", $lastUserCheck)->count();
	}
}
