<?php

namespace App\Components;

use Nette\Application\UI\Control;

/**
 * Class BaseComponent
 * @package App\Components
 */
abstract class BaseComponent extends Control
{
	 /**
     * @var bool
     */
    protected $autoSetupTemplateFile = true;


    /**
     * BaseComponent constructor.
     * @param null $parent
     * @param null $name
     */
    public function __construct($parent = null, $name = null)
    {
        parent::__construct($parent, $name);
    }

    /**
     * default render
     */
    public function render()
    {
        $this->template->render();
    }


    /**
     * @return \Nette\Application\UI\ITemplate
     */
    protected function createTemplate()
    {
        $template = parent::createTemplate();

        if ($this->autoSetupTemplateFile) {
            $template->setFile($this->getTemplateFilePath());
        }

        return $template;
    }

    /**
     * @return string
     */
    protected function getTemplateFilePath()
    {
        $reflection = $this->getReflection();
        $dir = dirname($reflection->getFileName());
        $filename = $reflection->getShortName() . '.latte';
        return $dir . \DIRECTORY_SEPARATOR . $filename;
    }

}
