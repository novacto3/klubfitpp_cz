<?php

use Phinx\Seed\AbstractSeed;

class S001UserSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
	public function run()
	{
		$this->table("user")->insert([
			"id" => 1,
			"name" => "Tomáš",
			"surname" => "Nováček",
			"phone" => "+420 737 121 520",
			"username" => "novacto3",
			"password" => "$2a$07$6in7yvhnb3t9z9bml9k56OD.AcV7/MJV1R67vrquqsnFMj61IamHW",
			"email" => "novacto3@fit.cvut.cz",
			"role" => "admin",
			"birthday" => "1994-05-23",
			"birthplace" => "Praha",
			"address" => "Vožická 970/1, Praha 4",
			"birth_number" => "940523/0131",
			"active" => true,
		])->save();

		$this->table("user")->insert([
			"id" => 2,
			"name" => "Stanislav",
			"surname" => "Jeřábek",
			"phone" => "+420 731 923 101",
			"username" => "jerabst1",
			"password" => "$2a$07$0dvikbu6nqyh1mxzjpnfnOyKUrwsDJE4VVOeGaIM5ElQUJtjL6jb2",
			"email" => "jerabst1@fit.cvut.cz",
			"role" => "admin",
			"birthday" => "1990-10-14",
			"birthplace" => "Vysoké Mýto",
			"address" => "České Heřmanice 112",
			"birth_number" => "901014/3945",
			"active" => true,
		])->save();

		$this->table("user")->insert([
			"id" => 3,
			"name" => "Jiří",
			"surname" => "Hanuš",
			"phone" => "+420 608 038 811",
			"username" => "hanusji8",
			"password" => "$2a$07$0dvikbu6nqyh1mxzjpnfnOyKUrwsDJE4VVOeGaIM5ElQUJtjL6jb2",
			"email" => "hanusji8@fit.cvut.cz",
			"role" => "admin",
			"birthday" => "1994-12-22",
			"birthplace" => "České Budějovice",
			"address" => "Náměstí ČSA 25, Hluboká n. Vlt.",
			"birth_number" => "941222/1291",
			"active" => true,
		])->save();


		$this->table("user")->insert([
			"id" => 4,
			"name" => "Test",
			"surname" => "User",
			"phone" => "+420 737 121 520",
			"username" => "testuser",
			"password" => "$2a$07$0dvikbu6nqyh1mxzjpnfnOyKUrwsDJE4VVOeGaIM5ElQUJtjL6jb2",
			"email" => "novacto3@fit.cvut.cz",
			"role" => "user",
			"birthday" => "1994-05-23",
			"birthplace" => "Praha",
			"address" => "Vožická 970/1, Praha 4",
			"birth_number" => "940523/0131",
			"active" => true,
		])->save();

    }
}
