<?php
namespace entities;

use \Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.4.17
 * Time: 19:31
 */
class User extends ActiveRow
{
	/* @var int id*/
	public $id;

	/* @var string username*/
	public $username;

	/* @var string name*/
	public $name;

	/* @var string surname*/
	public $surname;

	/* @var string password*/
	public $password;

	/* @var string email*/
	public $email;

	/* @var string phone*/
	public $phone;

	/* @var DateTime birthday*/
	public $birthday;

	/* @var string birthplace*/
	public $birthplace;

	/* @var string address*/
	public $address;

	/* @var string birth_number*/
	public $birth_number;

	/* @var string role*/
	public $role;

	/* @var int active*/
	public $active;

	/* @var int signed*/
	public $signed;

	/* @var int pr*/
	public $pr;

	/* @var DateTime created*/
	public $created;

	/* @var DateTime updated*/
	public $updated;

	/* @var DateTime last_user_check*/
	public $last_user_check;

	/* @var DateTime signed_time*/
	public $signed_time;
}
