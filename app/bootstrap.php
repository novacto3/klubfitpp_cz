<?php

require __DIR__ . '/../vendor/autoload.php';



$configurator = new Nette\Configurator;

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->addDynamicParameters(['env' => $_ENV]);

$enableDebugger = filter_var(getenv('DEBUG'), FILTER_VALIDATE_BOOLEAN);


// Enable Nette Debugger for error visualisation & logging
$configurator->setDebugMode($enableDebugger);
\Tracy\Debugger::$strictMode = true;
\Tracy\Debugger::$productionMode = !$enableDebugger;

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');

define('__MAIL_DIR__', __DIR__ . '/templates/Mails');
define('__PDF_DIR__', __DIR__ . '/templates/PDF');
define('__SIGN__', __DIR__.'/../www/pdf/sign');

Vodacek\Forms\Controls\DateInput::register();

Nette\Forms\Controls\BaseControl::enableAutoOptionalMode();

Nette\Forms\Validator::$messages[] = [
	Nette\Application\UI\Form::FILLED => 'Položka „%label“ musí být vyplněna.',
	Nette\Application\UI\Form::EMAIL => 'Položka „%label“ nemá správný formát e-mailu.',
	Nette\Application\UI\Form::URL => 'Položka „%label“ nemá správný formát.',
	Nette\Application\UI\Form::MAX_LENGTH => 'Položka „%label“ je příliš dlouhá. Maximální délka je %d znaků.',
	Nette\Application\UI\Form::MIN_LENGTH => 'Položka „%label“ je příliš krátká. Minimální délka je %d znaků.',
	Nette\Application\UI\Form::INTEGER => 'Položka „%label“ musí být celé číslo.',
	//\Nette\Application\UI\Form::REGEXP => 'Neplatný formát položky „%label“.',
	Nette\Application\UI\Form::PATTERN => 'Neplatný formát položky „%label“.',
	Nette\Application\UI\Form::RANGE => 'V položce „%label“ může být pouze číslo v rozsahu %d až %d.',
];

$container = $configurator->createContainer();
$container->getService('application')->errorPresenter = 'Error';

return $container;


