<?php

namespace App\Components;


use Nette\Application\UI\Form;
use Vodacek\Forms\Controls\DateInput;

/**
 * Class UserEditComponent
 * @package App\Components
 */
class UserEditComponent extends BaseComponent
{

	/**
	 * @inject
	 * @var \DB\UserRepository
	 */
	public $userRepository;

    private $userId;

    public function __construct($userId)
    {
        parent::__construct();

        $this->userId = $userId;
    }

    protected function createComponentUserEdit()
    {
        $editUser = $this->userRepository->findById($this->userId);

        $form = new Form();
        $form->addText('name', 'Jméno')
            ->setRequired('Zadejte své jméno')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka jména je %d znaků', 250);

        $form->addText('surname', 'Příjmení')
            ->setRequired('Zadejte své příjmení')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka příjmení je %d znaků', 250);

        $form->addText('email', 'Email')
            ->setEmptyValue('@')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka emailu je %d znaků', 30)
            ->addRule(Form::EMAIL, 'Zadán neplatný email.');

        $form->addText('username', 'Username')
            ->setRequired('Zadejte své uživatelské jméno')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka username je %d znaků', 20);

        $form->addText('phone', 'Telefon')
            ->setEmptyValue('+420')
            ->setRequired('Zadejte svůj telefon')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka telefonního čísla je %d znaků', 25);

	    $form->addDate('birthday', 'Datum narození', DateInput::TYPE_DATE)
		    ->setRequired();

	    $form->addText('birthplace', 'Místo narození')
		    ->addRule(Form::MAX_LENGTH, 'Maximální délka místa narození je %d znaků', 250)
		    ->setRequired();

	    $form->addText('address', 'Trvalé bydliště')
		    ->addRule(Form::MAX_LENGTH, 'Maximální délka trvalého bydliště je %d znaků', 250)
		    ->setRequired();

	    $form->addCheckbox("pr", "Chci pomáhat na PR akcích FIT ČVUT")
		    ->setDefaultValue(true);

        $form->addHidden('id');

        $form->addSubmit('send', 'Editovat')
            ->setAttribute('class', 'pure-button pure-button-primary');

        $form->setDefaults($editUser);

        $form->onSuccess[] = [$this, "userEditSubmitted"];
	    $form->elementPrototype->addAttributes(['class' => 'pure-form']);
	    return $form;
    }

    public function userEditSubmitted(Form $form)
    {
        $values = $form->getValues();
        $this->userRepository->updateUser($values);
        $this->getPresenter()->flashMessage('Uživatel editován.', 'success');
        $this->getPresenter()->redirect('edit');
    }
}
