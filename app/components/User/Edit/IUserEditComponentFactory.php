<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 17.7.15
 * Time: 9:59
 */

namespace App\Components;

/**
 * Interface IUserEditComponentFactory
 * @package App\Components
 */
interface IUserEditComponentFactory
{

    /** @return UserEditComponent */
    function create($userId);
}
