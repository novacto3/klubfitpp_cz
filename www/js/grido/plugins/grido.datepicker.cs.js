/**
 * Grido date picker plugin.
 * @link https://github.com/dangrossman/bootstrap-daterangepicker
 *
 * @author Petr Bugyík, Modified by Tomáš Škvrna
 * @param {jQuery} $
 * @param {Window} window
 * @param {undefined} undefined
 */
;
(function($, window, undefined) {
    /*jshint laxbreak: true, expr: true */
    "use strict";

    window.Grido.Grid.prototype.onInit.push(function(Grido)
    {
        if ($.fn.daterangepicker === undefined) {
            console.error('Plugin "bootstrap-daterangepicker.js" is missing! Run `bower install bootstrap-daterangepicker` and load it.');
            return;
        }

        Grido.$element.on('focus', 'input.date', function() {
            $(this).daterangepicker(
            {
                singleDatePicker: true,
                showDropdowns: true,
                format: "DD.MM.YYYY",
				"locale": {
					"format": "DD.MM.YYYY",
					"separator": " - ",
					"applyLabel": "Použít",
					"cancelLabel": "Storno",
					"fromLabel": "Od",
					"toLabel": "Do",
					"customRangeLabel": "Volitelně",
					"daysOfWeek": [
						"Ne",
						"Po",
						"Út",
						"St",
						"Čt",
						"Pá",
						"So"
					],
					"monthNames": [
						"Leden",
						"Únor",
						"Březen",
						"Duben",
						"Květen",
						"Červen",
						"Červenec",
						"Srpen",
						"Září",
						"Říjen",
						"Listopad",
						"Prosinec"
					],
					"firstDay": 1
				},
			});
		});
	});

})(jQuery, window);
