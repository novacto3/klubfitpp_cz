<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

        $router[] = new Route('sEGGfault', [
            'presenter' => 'Seggfault',
            'action' => 'default',
        ]);

        $router[] = new Route('sEGGfault/8b04d5e', [
            'presenter' => 'Seggfault',
            'action' => 'first',
        ]);

        $router[] = new Route('sEGGfault/a9f0e61', [
            'presenter' => 'Seggfault',
            'action' => 'second',
        ]);

        $router[] = new Route('sEGGfault/dd5c8bf', [
            'presenter' => 'Seggfault',
            'action' => 'third',
        ]);

        $router[] = new Route('sEGGfault/c0759f2', [
            'presenter' => 'Seggfault',
            'action' => 'fourth',
        ]);

        $router[] = new Route('sEGGfault/0883a65', [
            'presenter' => 'Seggfault',
            'action' => 'fifth',
        ]);

        $router[] = new Route('sEGGfault/93c38fc', [
            'presenter' => 'Seggfault',
            'action' => 'sixth',
        ]);

        $router[] = new Route('sEGGfault/5098d24', [
            'presenter' => 'Seggfault',
            'action' => 'seventh',
        ]);

        $router[] = new Route('prihlaseni', [
            'presenter' => 'Sign',
            'action' => 'in',
        ]);

		$router[] = new Route('prihlaseni', [
			'presenter' => 'Sign',
			'action' => 'in',
		]);

		$router[] = new Route('zapomenute-heslo', [
			'presenter' => 'Sign',
			'action' => 'forgot',
		]);

		$router[] = new Route('registrace', [
			'presenter' => 'User',
			'action' => 'registration',
		]);

		$router[] = new Route('administrace', [
			'presenter' => 'User',
			'action' => 'default',
		]);

		$router[] = new Route('muj-ucet/editace', [
			'presenter' => 'User',
			'action' => 'edit',
		]);

		$router[] = new Route('informace', [
			'presenter' => 'Homepage',
			'action' => 'info',
		]);

		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}

}
