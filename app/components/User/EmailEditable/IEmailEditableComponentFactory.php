<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 17.7.15
 * Time: 9:59
 */

namespace App\Components;

/**
 * Interface IEmailEditableComponentFactory
 * @package App\Components
 */
interface IEmailEditableComponentFactory
{

    /** @return EmailEditableComponent */
    function create();
}
