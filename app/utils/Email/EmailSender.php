<?php


namespace Utils\Email;

use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Object;

/**
 * Class Email for sending emails
 * @package Utils\Email
 */
class Email extends Object
{

    /**
     * @var \Nette\Mail\IMailer
     * classic SMTP Mailer
     */
    public $smtpMailer;
    public $sender;

    const SENDMAIL = "SENDMAIL";


	/**
	 * Email constructor.
	 * @param \Nette\Mail\IMailer $mailer
	 */
	public function __construct(IMailer $mailer)
    {
        $this->smtpMailer = $mailer;
    }

    /**
     * Prepares and sends mail
     * @param $email string email where message will be send to
     * @param $subject string subject of email
     * @param $text string text of email
     * @param $sender string who sends the email
     */
    public function sendMail($email, $subject, $text, $sender)
    {
        $mail = new Message;
        $mail->setSubject($subject);
        $mail->setFrom('predstavenstvo@klubfitpp.cz', 'FIT++');
        $text .= '<br><br>' . $sender;
        $mail->setHtmlBody($text);
        $mail->addTo($email);

        $this->smtpMailer->send($mail);
    }

	/**
	 * @param $emails
	 * @param $subject
	 * @param $text
	 * @param $sender
	 */
	public function sendCollectiveMail($emails, $subject, $text, $sender)
    {
        $mail = new Message;
        $mail->setSubject($subject);
        $mail->setFrom('predstavenstvo@klubfitpp.cz', 'FIT++');
        $text .= '<br><br>' . $sender;
        $mail->setHtmlBody($text);

        foreach ($emails as $value) {
            $mail->addTo($value);
        }

        $this->smtpMailer->send($mail);
    }

	/**
	 * @param $template
	 * @param $subject
	 * @param $recipients
	 */
	public function sendMailFromTemplate($template, $subject, $recipients)
    {
        $mail = new Message;
        $mail->setSubject($subject);
        $mail->setFrom('predstavenstvo@klubfitpp.cz', 'FIT++');
        $mail->setHtmlBody($template);

        foreach ($recipients as $value) {
            $mail->addTo($value);
        }

        $this->smtpMailer->send($mail);
    }

	/**
	 * @param $sender
	 */
	public function setSender($sender)
    {
        $this->sender = $sender;
    }
}
