<?php

/** @return mixed */
function throwFunction($e){
    throw $e;
}

return
    [
        'paths' => [
            'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
            'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds',
        ],

        'environments' => [
            'default_migration_table' => 'phinxlog',
            'default_database' => 'env',
            'env' => [
                'adapter' => 'mysql',
                'host' => getenv('DB_HOST') ?: throwFunction(new \RuntimeException('Missing ENV var DB_HOST')),
                'name' => getenv('DB_NAME') ?: throwFunction(new \RuntimeException('Missing ENV var DB_NAME')),
                'user' => getenv('DB_USER') ?: throwFunction(new \RuntimeException('Missing ENV var DB_USER')),
                'pass' => getenv('DB_PASS') ?: throwFunction(new \RuntimeException('Missing ENV var DB_PASS')),
                'port' => '3306',
                'charset' => 'utf8',
            ],
        ],
    ];
  
